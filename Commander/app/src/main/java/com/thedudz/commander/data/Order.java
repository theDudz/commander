package com.thedudz.commander.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Razvan Bugariu on 5/15/2016.
 */
public class Order {

    List<OrderedProduct> orderedProducts;

    public Order() {
        orderedProducts = new ArrayList<>();
    }

    public List<OrderedProduct> getOrderedProducts() {
        return orderedProducts;
    }

    public boolean addProduct(OrderedProduct orderedProduct){
        return orderedProducts.add(orderedProduct);
    }

}
