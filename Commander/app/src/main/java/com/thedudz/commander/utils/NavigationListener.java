package com.thedudz.commander.utils;

import android.support.v4.app.Fragment;

/**
 * Created by Razvan Bugariu on 5/14/2016.
 */
public interface NavigationListener {

    public void navigateTo(Fragment fragment);

}
