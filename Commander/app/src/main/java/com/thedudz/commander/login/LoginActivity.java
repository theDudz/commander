package com.thedudz.commander.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.thedudz.commander.R;
import com.thedudz.commander.home.HomeActivity;
import com.thedudz.commander.utils.CONSTANTS;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUIComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(userLoggedIn()){
            goToHome();
        }
    }

    private boolean userLoggedIn() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        long userID = sharedPreferences.getLong(CONSTANTS.USER_ID, CONSTANTS.DEFAULT_ID);
        if(userID > -1 ) {
            return true;
        }
        return false;
    }

    private void initUIComponents(){
        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(this);
    }

    private void login(){
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(CONSTANTS.EMAIL, editTextUsername.getText().toString());
            jsonObject.put(CONSTANTS.PASSWORD, editTextPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, CONSTANTS.URL + "/" + CONSTANTS.USER, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status;
                    status = response.getString(CONSTANTS.RESPONSE_STATUS);
                    long id = -1;
                    if(status.equals("ok")){
                        id = response.getLong(CONSTANTS.USER_ID.toLowerCase());
                    }
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    sharedPreferences.edit().putLong( CONSTANTS.USER_ID, id).commit();
                    goToHome();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "The request has not been sent. Please try again later", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(postRequest);
    }

    private  void showDialog()
    {
        new MaterialDialog.Builder(this)
                .title(R.string.progress_dialog)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void goToHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonLogin: {
                if(validateFields()) {
                    login();
                    showDialog();
                } else {
                    Toast.makeText(getApplicationContext(), "Please recheck your data!", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private boolean validateFields() {
        String input_username = editTextUsername.getText().toString();
        String input_password = editTextPassword.getText().toString();
        if(TextUtils.isEmpty(input_username) && TextUtils.isEmpty(input_password))
            return false;
        return true;
    }
}
