package com.thedudz.commander.home;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.thedudz.commander.R;
import com.thedudz.commander.data.Category;
import com.thedudz.commander.data.OrderedProduct;
import com.thedudz.commander.data.Product;
import com.thedudz.commander.utils.AddProductListener;
import com.thedudz.commander.utils.CONSTANTS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsFragment extends Fragment {

    private ListView listViewProducts;
    private ArrayAdapter<String> arrayAdapter;
    private Category selectedCategory;
    private AddProductListener productListener;
    private List<String> productNames;
    private List<Product> products;
    private MaterialDialog materialDialog;
    private OrderedProduct orderedProduct;
    private MaterialDialog loadingMaterialDialog;

    @SuppressLint("ValidFragment")
    private ProductsFragment() {
    }

    public static ProductsFragment newInstance(Category category) {
        ProductsFragment fragment = new ProductsFragment();
        fragment.selectedCategory = category;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        init();
        getProducts();
        View layout = inflater.inflate(R.layout.fragment_products, container, false);
        listViewProducts = (ListView) layout.findViewById(R.id.listViewProducts);
        return layout;
    }

    public void init(){
        productListener = (AddProductListener) getActivity();
        productNames = new ArrayList<String>();
        products = new ArrayList<Product>();
        orderedProduct = new OrderedProduct();
        loadingMaterialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.progress_data)
                .content(R.string.please_wait)
                .progress(true, 0)
                .build();
    }

    public void getProducts() {
        loadingMaterialDialog.show();
        RequestQueue queue = Volley.newRequestQueue(getContext());

        Log.d("URL", selectedCategory.toString());

        JsonObjectRequest categoriesRequest = new JsonObjectRequest(Request.Method.GET, CONSTANTS.URL + "/" + CONSTANTS.PRODUCTS + "/" + selectedCategory.getId(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString(CONSTANTS.STATUS).equals(CONSTANTS.OK_STATUS));
                    {
                        JSONArray productsJsonArray = response.getJSONArray(CONSTANTS.PRODUCTS_RO);
                        int jsonArrayLength = productsJsonArray.length();
                        for(int i = 0 ; i < jsonArrayLength; i++){
                            Product product = new Product();
                            JSONObject productJsonObject = (JSONObject) productsJsonArray.get(i);
                            product.setId(productJsonObject.getLong(CONSTANTS.ITEM_ID));
                            product.setDescription(productJsonObject.getString(CONSTANTS.ITEM_DESCRIPTION));
                            product.setCategoryID(selectedCategory.getId());
                            product.setPrice(productJsonObject.getDouble(CONSTANTS.PRODUCT_PRICE));
                            products.add(product);
                            productNames.add(product.getDescription());
                        }
                    }
                    loadingMaterialDialog.dismiss();
                    initListView();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("CATEGORIES", error.toString());
                Toast.makeText(getContext(), "Error!", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(categoriesRequest);

    }

    private void initListView() {
        arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.products_row, R.id.rowProductName, productNames);
        listViewProducts.setAdapter(arrayAdapter);
        listViewProducts.isClickable();
        listViewProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Product selectedProduct = products.get(position);
                materialDialog = new MaterialDialog.Builder(getContext())
                                    .title(selectedProduct.getDescription())
                                    .content("Price +" + selectedProduct.getPrice())
                                    .inputRangeRes(1, 20, R.color.yellow)
                                    .positiveText(R.string.agree)
                                    .negativeText(R.string.disagree)
                                    .onPositive(new MaterialDialog.SingleButtonCallback(){
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            orderedProduct.setPrice(selectedProduct.getPrice());
                                            orderedProduct.setObservation(selectedProduct.getDescription());
                                            orderedProduct.setId(selectedProduct.getId());
                                            productListener.addProductToOrder(orderedProduct);
                                                    dialog.dismiss();
                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .inputType(InputType.TYPE_CLASS_NUMBER)
                                    .input(R.string.quantity_hint, R.string.input_prefill, new MaterialDialog.InputCallback() {
                                        @Override
                                        public void onInput(MaterialDialog dialog, CharSequence input) {
                                            Log.d("QUAT", input.toString());
                                            int quantity = Integer.parseInt(input.toString());
                                            orderedProduct.setQuantity(quantity);
                                        }
                                    }).build();
                materialDialog.show();
            }
        });
    }

}
