package com.thedudz.commander.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.thedudz.commander.R;
import com.thedudz.commander.data.Category;
import com.thedudz.commander.data.Order;
import com.thedudz.commander.data.OrderedProduct;
import com.thedudz.commander.login.LoginActivity;
import com.thedudz.commander.order.OrdersFragment;
import com.thedudz.commander.payments.PaymentsFragment;
import com.thedudz.commander.utils.AddProductListener;
import com.thedudz.commander.utils.CONSTANTS;
import com.thedudz.commander.utils.NavigationListener;

public class HomeActivity extends AppCompatActivity implements NavigationListener, NavigationView.OnNavigationItemSelectedListener, AddProductListener {

    private Toolbar mToolbar ;
    private HomeReaderFragment readerFragment;
    private PaymentsFragment paymentsFragment;
    private HomeMenuFragment menuFragment;
    private OrdersFragment ordersFragment;
    private Category selectedCategory = new Category();
    private NavigationView navigationView;
    private Order order;
    private ImageButton waiterImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initNewOrder();
        initMainUiComponents();
        deactiveWaiter();
        initNavigationView();
        initDrawerLayout();
    }

    private void initNewOrder(){
        order = new Order();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferences.edit().clear().commit();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ( checkTable() ) {
            addAsCurrentUi(menuFragment);
        } else {
            addAsCurrentUi(readerFragment);
        }
    }

    private boolean checkTable(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sharedPreferences.getLong(CONSTANTS.TABLE_NUMBER, CONSTANTS.DEFAULT_INT) >= 0) {
            return true;
        } else {
            return false;
        }
    }

    private void initMainUiComponents() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        waiterImageButton = (ImageButton) findViewById(R.id.waiterImageButton);
        waiterImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goTOOrders();
            }
        });
        readerFragment = HomeReaderFragment.newInstance();
        menuFragment = HomeMenuFragment.newInstance(selectedCategory);
        paymentsFragment = PaymentsFragment.newInstance();
        ordersFragment = OrdersFragment.newInstance(order);
    }

    private void goTOOrders() {
        if(!order.getOrderedProducts().isEmpty()) {
            addAsCurrentUi(ordersFragment);
        } else {
            Toast.makeText(getApplicationContext(), "Add some items in the order", Toast.LENGTH_SHORT).show();
        }
    }

    private void addAsCurrentUi(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.ui_hook, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void navigateTo(Fragment fragment) {
        addAsCurrentUi(fragment);
    }

    private void initDrawerLayout() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void initNavigationView() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_pay_id : {
                if( checkTable() ) {
                    addAsCurrentUi(paymentsFragment);
                } else {
                    Toast.makeText(getApplicationContext(), "Please, scan your code!", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.menu_menu_id : {
                if( checkTable() ) {
                    addAsCurrentUi(menuFragment);
                } else {
                    Toast.makeText(getApplicationContext(), "Please, scan your code!", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.menu_logout : {
                logout();
                break;
            }
            case R.id.menu_orders : {
                if( checkTable() ) {
                    goTOOrders();
                } else {
                    Toast.makeText(getApplicationContext(), "Please, scan your code!", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    @Override
    public void addProductToOrder(OrderedProduct product) {
        order.addProduct(product);
        activeWaiter();
    }

    private void activeWaiter(){
        waiterImageButton.setVisibility(View.VISIBLE);
    }

    private void deactiveWaiter(){
        waiterImageButton.setVisibility(View.INVISIBLE);
    }

}