package com.thedudz.commander.home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.thedudz.commander.R;
import com.thedudz.commander.data.Category;
import com.thedudz.commander.utils.CONSTANTS;
import com.thedudz.commander.utils.NavigationListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeMenuFragment extends Fragment {

    private ListView listViewCategories;
    private List<String> categoriesNames;
    private ArrayAdapter<String> arrayAdapter;
    private Category selectedCategory;
    private List<Category> categoriesList;
    private NavigationListener navigationListener;
    private MaterialDialog materialDialog;

    public HomeMenuFragment() {
    }

    public static HomeMenuFragment newInstance(Category selectedCategory) {
        HomeMenuFragment fragment = new HomeMenuFragment();
        fragment.selectedCategory = selectedCategory;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_home_menu, container, false);
        listViewCategories = (ListView) layout.findViewById(R.id.listViewCategory);
        init();
        iniComponents();
        return layout;
    }

    private void init() {
        categoriesNames = new ArrayList<>();
        categoriesList = new ArrayList<>();
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.progress_data)
                .content(R.string.please_wait)
                .progress(true, 0)
                .build();
    }

    @Override
    public void onResume() {
        super.onResume();
        selectedCategory = null;
        init();
        getCategories();
    }

    private void iniComponents() {
        navigationListener = (HomeActivity) getActivity();
    }

    private void initListView() {
        arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.category_row, R.id.rowCategoryName, categoriesNames);
        listViewCategories.setAdapter(arrayAdapter);
        listViewCategories.isClickable();
        listViewCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedCategory = categoriesList.get(position);
                navigationListener.navigateTo(ProductsFragment.newInstance(selectedCategory));
            }
        });
    }

    @Override
    public void onPause() {
        arrayAdapter.clear();
        super.onPause();
    }

    public void getCategories() {
        materialDialog.show();
        RequestQueue queue = Volley.newRequestQueue(getContext());

        JsonObjectRequest categoriesRequest = new JsonObjectRequest(Request.Method.GET, CONSTANTS.URL + "/" + CONSTANTS.CATEGORIES, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString(CONSTANTS.STATUS).equals(CONSTANTS.OK_STATUS));
                    {
                        JSONArray categoryJsonArray = response.getJSONArray(CONSTANTS.CATEGORIES);
                        int jsonArrayLength = categoryJsonArray.length();
                        for(int i = 0 ; i < jsonArrayLength; i++){
                            Category cat = new Category();
                            cat.setId(((JSONObject)categoryJsonArray.get(i)).getLong(CONSTANTS.ITEM_ID));
                            cat.setName(((JSONObject)categoryJsonArray.get(i)).getString(CONSTANTS.ITEM_DESCRIPTION));
                            categoriesList.add(cat);
                            categoriesNames.add(cat.getName());
                        }
                    }
                    materialDialog.dismiss();
                    initListView();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("CATEGORIES", error.toString());
            }
        });
        queue.add(categoriesRequest);
    }
}
