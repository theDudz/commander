package com.thedudz.commander.utils;

import com.thedudz.commander.data.OrderedProduct;

/**
 * Created by Razvan Bugariu on 5/15/2016.
 */
public interface AddProductListener {

    void addProductToOrder(OrderedProduct product);

}
