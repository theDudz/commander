package com.thedudz.commander.home;


import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.thedudz.commander.R;
import com.thedudz.commander.login.LoginActivity;
import com.thedudz.commander.utils.CONSTANTS;

import org.json.JSONException;
import org.json.JSONObject;

import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeReaderFragment extends Fragment {


    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 6969;
    private TextView textViewReader;
    private View layout;
    private SurfaceView surfaceView;
    private boolean isRunning = false;
    private MaterialDialog dialog;
    private String qrCodeData="";

    public HomeReaderFragment() {
        // Required empty public constructor
    }

    public static HomeReaderFragment newInstance() {
        HomeReaderFragment fragment = new HomeReaderFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        QREader.getInstance().start();
        isRunning = true;
    }

    @Override
    public void onPause() {
        super.onPause();

        QREader.getInstance().stop();
        isRunning = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        QREader.getInstance().releaseAndCleanup();
        isRunning = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_home_reader, container, false);
        initMainUiComponents();
        if (!(ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)) {
            getQRCode();
        } else {
            checkPermission();
        }
        return layout;
    }

    private void getQRCode() {
        QREader.getInstance().setUpConfig(new QRDataListener() {
            @Override public void onDetected(final String data) {
                Log.d("QREader", "Value : " + data);
                textViewReader.post(new Runnable() {
                    @Override public void run() {
                        qrCodeData=data;
                        textViewReader.setText(data);
                        getTable();
                    }
                });
            }
        });
        QREader.getInstance().init(getContext(), surfaceView);
    }

    private void getTable() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(CONSTANTS.QR,qrCodeData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, CONSTANTS.URL + "/" + CONSTANTS.TABLE, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status;
                    status = response.getString(CONSTANTS.RESPONSE_STATUS);
                    long id = -1;
                    if(status.equals("ok")){
                        dialog.show();
                        id = response.getInt(CONSTANTS.TABLE_ID);
                    }
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                    sharedPreferences.edit().putLong( CONSTANTS.TABLE_NUMBER, id).commit();
                    goToLogin();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "The request has not been sent. Please try again later", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(postRequest);
    }

    private void goToLogin() {
        dialog.dismiss();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }


    private void initMainUiComponents() {
        checkPermission();
        textViewReader = (TextView) layout.findViewById(R.id.textViewRead);
        surfaceView = (SurfaceView) layout.findViewById(R.id.camera_view);

        dialog = new MaterialDialog.Builder(getContext())
                .title(R.string.processing_data)
                .content(R.string.please_wait)
                .progress(true, 0)
                .build();
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   getQRCode();
                } else {
                    goToLogin();
                }
                return;
            }
        }
    }

}
