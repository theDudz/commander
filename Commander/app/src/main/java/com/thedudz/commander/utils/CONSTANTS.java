package com.thedudz.commander.utils;

/**
 * Created by Razvan Bugariu on 5/13/2016.
 */
public class CONSTANTS {

    //Volley calls
    public static final String URL = "http://mobile.itec.ligaac.ro";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String USER = "user";
    public static final String RESPONSE_STATUS = "status";
    public static final String CATEGORIES = "categories";
    public static final String STATUS = "status";
    public static final String OK_STATUS = "ok";
    public static final String ITEM_ID = "id";
    public static final String ITEM_DESCRIPTION = "description";
    public static final String PRODUCTS = "products";
    public static final String PRODUCT_PRICE = "price";
    public static final String PRODUCTS_RO = "produse";
    public static final String QR = "qr";
    public static final String TABLE = "table";


    //Shared Preferences
    public static final String USER_ID = "USER_ID";
    public static final String DEFAULT_STRING = "DEFAULT_VALUE";

    public static final long DEFAULT_ID = -1;
    public static final String TABLE_ID = "tableId";
    public static final String TABLE_NUMBER = "table_number";

    public static final int DEFAULT_INT = -1;
    public static final long DEFAULT_LONG = -1;
}
