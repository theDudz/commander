package com.thedudz.commander.order;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.thedudz.commander.R;
import com.thedudz.commander.data.Category;
import com.thedudz.commander.data.Order;
import com.thedudz.commander.data.OrderedProduct;
import com.thedudz.commander.home.ProductsFragment;
import com.thedudz.commander.utils.CONSTANTS;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrdersFragment extends Fragment implements View.OnClickListener {

    private Order order;
    private View layout;
    private ListView listView;
    private Button orderButton;
    private ArrayAdapter<String> arrayAdapter;
    private List<String> ordersDescriptions;
    private MaterialDialog materialDialog;

    public static OrdersFragment newInstance(Order order) {
        OrdersFragment fragment = new OrdersFragment();
        fragment.order = order;
        return fragment;
    }

    public OrdersFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_orders, container, false);
        initUIMainComponents();
        initDescriptions();
        initListView();
        return layout;
    }

    private void initDescriptions() {
        ordersDescriptions = new ArrayList<>();
        for(OrderedProduct orderedProduct : order.getOrderedProducts()){
            ordersDescriptions.add(orderedProduct.getObservation() + "           " + orderedProduct.getQuantity() + " pc/pcs" + "       "  + "Price :  " + orderedProduct.getPrice() + "$");
        }
    }

    private void initUIMainComponents() {
        listView = (ListView) layout.findViewById(R.id.ordersListView);
        orderButton = (Button) layout.findViewById(R.id.buttonOrder);
        orderButton.setOnClickListener(this);
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.progress_data)
                .content(R.string.please_wait)
                .progress(true, 0)
                .build();
    }

    private void initListView() {
        arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.orders_row, R.id.rowOrderDetailsName, ordersDescriptions);
        listView.setAdapter(arrayAdapter);
        listView.isClickable();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonOrder : {
                orderRequest();
            }
        }
    }

    private void orderRequest() {

        materialDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getContext());

        JSONObject jsonObject = new JSONObject();

        JSONObject[] jsonObjects = getMeJSONI();

        try {
            jsonObject.put("", jsonObjects);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        long userID = sharedPreferences.getLong(CONSTANTS.USER_ID, CONSTANTS.DEFAULT_ID);
        long tableID = sharedPreferences.getLong(CONSTANTS.TABLE_NUMBER, CONSTANTS.DEFAULT_ID);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, CONSTANTS.URL + "/order/" + userID + "/" + tableID, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getContext(), response.getString("status").toString() + ", " + response.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                materialDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                materialDialog.dismiss();
                Toast.makeText(getContext(), "The request has not been sent. Please try again later", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(postRequest);

    }

    private JSONObject[] getMeJSONI() {
        JSONObject[] jsonObjects = new JSONObject[order.getOrderedProducts().size()];

        int i = 0 ;
        for(OrderedProduct object : order.getOrderedProducts()){
            JSONObject newObject = new JSONObject();
            try {
                newObject.put("productId", object.getId());
                newObject.put("quantity", object.getQuantity());
                newObject.put("observations", "NONE");
                jsonObjects[i++] = newObject;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return jsonObjects;
    }
}
