package com.thedudz.commander.payments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thedudz.commander.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentsFragment extends Fragment {


    public PaymentsFragment() {
    }

    public static PaymentsFragment newInstance() {
        PaymentsFragment fragment = new PaymentsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_payments, container, false);
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("PAYMENTS", "ASAS");
    }
}
